using System.Diagnostics;
using DeLaSalle.Ecommerce.Api.Repositories.Interfaces;
using DeLaSalle.Ecommerce.Core.Entities;
using DeLaSalle.Ecommerce.Core.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeLaSalle.Ecommerce.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ProductCategoriesController : ControllerBase
{
    private readonly IProductCategoryRepository _productCategoryRepository;
    
    public ProductCategoriesController(IProductCategoryRepository productCategoryRepository)
    {
        _productCategoryRepository = productCategoryRepository;
    }
    
    [HttpGet]
    public async Task<ActionResult<Response<List<ProductCategory>>>> GetAll()
    {
        var categories = await _productCategoryRepository.GetAllAsync();
        var response = new Response<List<ProductCategory>>();
        response.Data = categories;
        
        return Ok(response);
    }
    
    [HttpPost]
    public async Task<ActionResult<Response<ProductCategory>>> Post([FromBody] ProductCategory category)
    {
        category = await _productCategoryRepository.SaveAsync(category);
       
        var response = new Response<ProductCategory>();
        response.Data = category;
        
        return Created($"/api/[controler]/{category.Id}",response);
    }

    /*GET         /api/productcategories              Obtener todas las categorías
 GET         /api/productcategories/{id}        Obtener categoria por Id
 POST      /api/productcategories               Guardar categoria
 PUT         /api/productcategories               Actualizar categoria
 DELETE  /api/productcategories               Dar de baja categoria
 */
}
