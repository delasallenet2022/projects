using DeLaSalle.Ecommerce.Api.Repositories.Interfaces;
using DeLaSalle.Ecommerce.Core.Entities;

namespace DeLaSalle.Ecommerce.Api.Repositories;

public class InMemoryProductCategoryRepository : IProductCategoryRepository
{
    private readonly List<ProductCategory> _categories;

    public InMemoryProductCategoryRepository()
    {
        _categories = new List<ProductCategory>();
    }
    
    public async Task<List<ProductCategory>> GetAllAsync()
    {
        // var lst = new List<ProductCategory>();
        // lst.Add(new ProductCategory{ Id = 1, Name = "Test", Description = "Test"});
        // lst.Add(new ProductCategory{ Id = 2, Name = "Test2", Description = "Test 2"});

        return _categories;
    }

    public async Task<ProductCategory> SaveAsync(ProductCategory category)
    {
        category.Id = _categories.Count + 1;
        _categories.Add(category);

        return category;
    }

    public Task<ProductCategory> UpdateAsync(ProductCategory category)
    {
        throw new NotImplementedException();
    }

    public Task<bool> DeleteAsync(int id)
    {
        throw new NotImplementedException();
    }

    public Task<ProductCategory> GetById(int id)
    {
        throw new NotImplementedException();
    }
}